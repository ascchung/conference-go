from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_photo(city, state):
    try:
        headers = {
            "Authorization" : PEXELS_API_KEY
        }
        params = {
            "per_page": 1,
            "query": f"{city} {state}"
        }
        url = f"https://api.pexels.com/v1/search"
        response = requests.get(url, headers=headers, params=params)
        content = response.json()

        if "photos" in content and content['photos']:
            return {"picture_url": content['photos'][0]['src']['original']}
        else:
            return {"error": "Invalid format in Pexels API response"}

    except requests.RequestException as e:
        return {"error": f"Pexels API request failed: {str(e)}"}


def get_weather_data(city, state):
    try:
    # Use the Open Weather API
    # Create the URL for the geocoding API with the city and state
        url = (

        "http://api.openweathermap.org/geo/1.0/direct?q="
        + city
        + ","
        + state
        + "&appid="
        + OPEN_WEATHER_API_KEY

        )
        # Make the request
        data_response = requests.get(url)
        data_response.raise_for_status()
        # Parse the JSON response
        data = json.loads(data_response.content)
        # Get the latitude and longitude from the response
        if data and isinstance(data, list) and len(data) > 0:
            lat = str(data[0]["lat"])
            lon = str(data[0]["lon"])

            # Create the URL for the current weather API with the latitude
            #   and longitude
            weather_url = (

                'https://api.openweathermap.org/data/2.5/weather?'
                + "lat="
                + lat
                + "&lon="
                + lon
                + "&appid="
                + OPEN_WEATHER_API_KEY
            )
            # Make the request
            weather_response = requests.get(weather_url)
            weather_response.raise_for_status()

            # Parse the JSON response
            weather_data = json.loads(weather_response.content)
            # Get the main temperature and the weather's description and put
            #   them in a dictionary
            temperature = weather_data['main']['temp']
            temperature = 1.8 * (int(temperature) - 273.16) + 32
            if "main" in weather_data and "weather" in weather_data:
                weather = {
                    "temperature": f"{int(temperature)} Fahrenheit",
                    "description": weather_data['weather'][0]['description'],
                }
                # Return the dictionary
                return weather
            else:
                return {"error": "Invalid format in Open Weather API response"}
        else:
            return {"error": "Invalid format in Open Weather API geocoding response"}
    except requests.RequestException as e:
            return {"error": f"Open Weather API request failed: {str(e)}"}
